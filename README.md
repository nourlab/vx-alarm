# `vx-alarm`

This repository hosts the code for the blog post *Vert.x clustering: a
distributed alarm system* hosted on
[buguigny.org](http://buguigny.org).

For every step in the post there is a dedicated branch for the code
named from `master-00`, `master-01`, ... `master-final`.

------

<small>
Initialized on 2020/04/12 with 
`groovy vep.groovy -pn vx-alarm -pkg org.buguigny.vertx_alarm -mfunc`.

vep v0.2.1 (2020.03.21) - See https://gitlab.com/mszmurlo/vep - MIT License
</small>
